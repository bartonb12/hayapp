@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">New Hay for Sale</div>

                <div class="card-body">
                  <form class="form-control" method="post">
                    @CSRF
                    <label for="hayName">Name:</label>
                    <input class="form-control" type="text" name="hayName" value="">

                    <label for="quantity">Quantity:</label>
                    <input class="form-control" type="text" name="quantity" value="">

                    <label for="hayType">Type:</label>
                    <input class="form-control" type="text" name="hayType" value="">

                    <label for="harvestTime">Harvest Date:</label>
                    <input class="form-control" type="text" name="harvestTime" value="">

                    <label for="harvestLocationId">Harvest Location:</label>
                    <input class="form-control" type="text" name="harvestLocationId" value="">

                    <label for="locationId">Location:</label>
                    <input class="form-control" type="text" name="locationId" value="">

                    <label for="maxDeliveryDistance">Max Delivery Distance:</label>
                    <input class="form-control" type="text" name="maxDeliveryDistance" value="">

                    <label for="deliveryNotes">Delivery Notes:</label>
                    <input class="form-control" type="text" name="deliveryNotes" value="">

                    <label for="haySize">Bale Size:</label>
                    <input class="form-control" type="text" name="haySize" value="">

                    <label for="pricePerBale">Price per Bale:</label>
                    <input class="form-control" type="text" name="pricePerBale" value="">

                    <label for="notes">Description:</label>
                    <input class="form-control" type="text" name="notes" value="">
                    <br>
                    <button type="submit" class="btn btn-primary" name="button">Sell my Hay!</button>
                  </form>


                </div>

            </div>
        </div>

    </div>
</div>
<br>
<br>
<br>
<br>
<br>
@endsection
