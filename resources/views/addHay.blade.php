@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                  <ul>
                  <li><a href="/newHay">Add New Hay for Sale</a></li>
                  <li><a href="/hayIndex">My Hay for Sale</a></li>
                  </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
