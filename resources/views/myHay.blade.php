@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                  <h1>My Hay</h1>
                  <table class="table">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">Hay Name</th>
                        <th scope="col">Hay Type</th>
                        <th scope="col">Price/Bale</th>
                        <th scope="col">Bale Size</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($hays as $hay)
                      <tr>
                        <td>{{ $hay->hayName }}</td>
                        <td>{{ $hay->hayType }}</td>
                        <td>{{ $hay->pricePerBale }}</td>
                        <td>{{ $hay->haySize }}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <a href="/newHay">Add New Hay</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
