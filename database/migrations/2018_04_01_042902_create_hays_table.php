<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hays', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('hayName');
            $table->string('hayType');
            $table->string('harvestTime');
            $table->string('deliveryNotes');
            $table->integer('maxDeliveryDistance');
            $table->string('notes');
            $table->string('haySize');
            $table->float('pricePerBale');
            $table->integer('harvestLocationId');
            $table->integer('locationId');
            $table->integer('userId');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hays');
    }
}
