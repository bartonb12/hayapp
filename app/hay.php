<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hay extends Model
{
  /**
 * The attributes that are mass assignable.
 *
 * @var array
 */
protected $fillable = [
  'hayName',
  'quantity',
  'hayType',
  'harvestTime',
  'harvestLocationId',
  'locationId',
  'maxDeliveryDistance',
  'deliveryNotes',
  'haySize',
  'pricePerBale',
  'notes',
];
}
