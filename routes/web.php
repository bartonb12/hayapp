<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/buyHay', 'HayController@search')->name('buyHay');

Route::get('/sellHay', 'HayController@addHay')->name('addHay');
Route::get('/newHay', 'HayController@newHay')->name('newHay');

Route::post('/newHay', 'HayController@storeHay')->name('postNewHay');

Route::get('/hayIndex', 'HayController@index')->name('myHay');

Route::get('/api', 'HayController@indexApi')->name('api');
