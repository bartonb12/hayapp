<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\hay;
use Auth;

class HayController extends Controller
{

    public function __construct()
    {
      $this->middleware('auth', ['except' => 'search']);
    }

    public function search()
    {
      $hays = hay::all();
      return view('search', compact('hays'));
    }

    public function addHay()
    {
      return view('addHay');
    }

    public function storeHay(Request $request)
    {
      $user = Auth::user();
      $hay = new hay;
      $hay->hayName             = $request->input('hayName');
      $hay->hayType             = $request->input('hayType');
      $hay->harvestTime         = $request->input('harvestTime');
      $hay->deliveryNotes       = $request->input('deliveryNotes');
      $hay->maxDeliveryDistance = $request->input('maxDeliveryDistance');
      $hay->notes               = $request->input('notes');
      $hay->haySize             = $request->input('haySize');
      $hay->pricePerBale        = $request->input('pricePerBale');
      $hay->harvestLocationId   = $request->input('harvestLocationId');
      $hay->locationId          = $request->input('locationId');
      $hay->userId              = Auth::user()->id;
      $hay->save();
      $hays = hay::all();
      return view('myHay', compact('hays'));
    }

    public function newHay()
    {
      $hay = new hay;
      $user = Auth::user();
      return view('newHay', compact('user', 'hay'));
    }

    public function index()
    {
      $hays = hay::where('userId', '=', Auth::user()->id)->get();
      return view('myHay', compact('hays'));
    }

    public function indexApi()
    {
      $hays = hay::where('userId', '=', Auth::user()->id)->get();
      return $hays;
    }
}
