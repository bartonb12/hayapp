@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a href="/buyHay">Buy Hay</a>
            <a href="/sellHay">Sell Hay</a>
        </div>
    </div>
</div>
@endsection
